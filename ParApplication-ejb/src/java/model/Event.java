
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Event implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String eventName;
    private int eventFee;
    private String eventDesc;

    public Event()
    {
    }

    public Event(String eventName, int eventFee, String eventDesc)
    {
        this.eventName = eventName;
        this.eventFee = eventFee;
        this.eventDesc = eventDesc;
    }

    public static List<Event> getEvents(EntityManager anEmanager)
    {
        String qry = "select o from Event o";
        List<Event> events = anEmanager.createQuery(qry).getResultList();
        return events;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getEventName()
    {
        return eventName;
    }

    public void setEventName(String eventName)
    {
        this.eventName = eventName;
    }

    public String getEventDesc()
    {
        return eventDesc;
    }

    public void setEventDesc(String eventDesc)
    {
        this.eventDesc = eventDesc;
    }

    public int getEventFee()
    {
        return eventFee;
    }

    public void setEventFee(int eventFee)
    {
        this.eventFee = eventFee;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (int) id;
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        if (!(object instanceof Event))
        {
            return false;
        }
        Event other = (Event) object;
        if (this.id != other.id)
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "model.Event[ id=" + id + " ]";
    }
}
