
package exercisePackage;

import java.applet.Applet;
import java.awt.Button;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Exercise1 extends Applet implements ActionListener
{
    Button btnClick;
    String message = "";
    
    @Override
    public void init()
    {
        btnClick = new Button("Click Me");
        btnClick.addActionListener(this);
        add(btnClick);
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
        message = "Ouch that really hurts haha";
        repaint();
    }
    
    @Override
    public void paint(Graphics g)
    {
        g.setFont(new Font("SanSerif", Font.ITALIC, 30));
        g.setColor(Color.red);
        g.drawString(message, 10, 50);
    }
}
