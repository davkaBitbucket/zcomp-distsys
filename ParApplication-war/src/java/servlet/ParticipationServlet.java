
package servlet;

import ejb.ParticipationSystemRemote;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name = "ParticipationServlet", urlPatterns =
{
    "/ParticipationServlet"
})
public class ParticipationServlet extends HttpServlet
{
    @EJB
    private ParticipationSystemRemote participationSystem;
    boolean addSampleData = true;

    protected void processRequest(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException
    {
        response.setContentType("text/html;charset=UTF-8");
        // Add sample data
        if (addSampleData)
        {
            participationSystem.addSampleData();
            addSampleData = false;
        }
        try
        {
            String action = "no action";
            try
            {
                InputStream inStream = request.getInputStream();
                ObjectInputStream inputFromApplet = 
                        new ObjectInputStream(inStream);
                action = (String) inputFromApplet.readObject();
                inStream.close();
                inputFromApplet.close();
            }
            catch (IOException e)
            {
                PrintWriter out = response.getWriter();
                out.println("<!DOCTYPE html>");
                out.println("<html>");
                out.println("<head>");
                out.println("<title>Servlet ParticipationServlet</title>");
                out.println("</head>");
                out.println("<body>");
                out.println("<h1>Servlet ParticipationServlet at "
                        + request.getContextPath() + "</h1>");
                out.println("</body>");
                out.println("</html>");
                out.close();
            }
            
            if (action.equals("start"))
            {
                participationSystem.startParticipation();
                ArrayList<String> eventNames = 
                        participationSystem.getEventNames();
                ArrayList<String> memberNames = 
                        participationSystem.getMemberNames();
                // Send event names and member names
                OutputStream outStream = response.getOutputStream();
                ObjectOutputStream outObjToApplet = 
                        new ObjectOutputStream(outStream);
                outObjToApplet.writeObject(eventNames);
                outObjToApplet.writeObject(memberNames);
                outObjToApplet.flush();
                outObjToApplet.close();
            }

            if (action.charAt(0) == 'e')
            {
                int index = Integer.parseInt(action.substring(1));
                participationSystem.selectEvent(index);
            }

            if (action.charAt(0) == 'm')
            {
                int index = Integer.parseInt(action.substring(1));
                String message = participationSystem.selectMember(index);
                // Send event name
                OutputStream outStream = response.getOutputStream();
                ObjectOutputStream outObjToApplet = 
                        new ObjectOutputStream(outStream);
                if (message.equals("notfullmember"))
                {
                    outObjToApplet.writeObject(message);
                }
                else
                {
                    outObjToApplet.
                            writeObject(participationSystem.getParDetails());
                }
                outObjToApplet.flush();
                outObjToApplet.close();
            }

            if (action.equals("close"))
            {
                participationSystem.closeParticipation();
            }
        }
        catch (ClassNotFoundException ex)
        {
            Logger.getLogger(ParticipationServlet.class.getName()).
                    log(Level.SEVERE, null, ex);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, 
    HttpServletResponse response) throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, 
    HttpServletResponse response)throws ServletException, IOException
    {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo()
    {
        return "Short description";
    }
}