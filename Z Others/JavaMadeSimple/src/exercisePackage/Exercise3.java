
package exercisePackage;

import java.applet.Applet;
import java.awt.Checkbox;
import java.awt.CheckboxGroup;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class Exercise3 extends Applet implements ItemListener
{
    Checkbox fontBold = new Checkbox("Bold", false);
    Checkbox fontItalic = new Checkbox("Italic", false);
    int bold = 0;
    int italic = 0;
    
    CheckboxGroup fontName = new CheckboxGroup();
    Checkbox sans = new Checkbox("Sans Serif", fontName, true);
    Checkbox serif = new Checkbox("Serif", fontName, false);
    Checkbox mono = new Checkbox("Monospaced", fontName, false);
    String fName = "SansSerif";
    
    public void init() 
    {
        add(fontBold);
        fontBold.addItemListener(this);
        add(fontItalic);
        fontItalic.addItemListener(this);
        add(sans);
        sans.addItemListener(this);
        add(serif);
        serif.addItemListener(this);
        add(mono);
        mono.addItemListener(this);
    }
    
    public void itemStateChanged(ItemEvent e)
    {
        String option = (String)e.getItem();
        if (option.equals("Bold"))
        {
            if (bold == 1) 
                bold = 0;
            else 
                bold = 1;
        }
        if (option.equals("Italic"))
        {
            if (italic == 2)
                italic = 0;
            else 
                italic = 2;
        }
        if (option.equals("Sans Serif"))
            fName = "Sans Serif";
        if (option.equals("Serif"))
            fName = "Serif";
        if (option.equals("Monospaced"))
            fName = "Monospaced";
        repaint();
    }
    
    public void paint(Graphics g)
    {
        g.setFont(new Font(fName, bold + italic ,18));
        g.drawString("This font is " + fName, 30, 50);
    }
}
