
package exercisePackage;

import java.applet.Applet;
import java.awt.Button;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Exercise5 extends Applet implements ActionListener
{
    TextArea source;
    TextArea destination;
    Button btnCopy;
    
    public void init() 
    {
        source = new TextArea(10, 30);
        add(source);
        btnCopy = new Button("Copy->");
        add(btnCopy);
        btnCopy.addActionListener(this);
        destination = new TextArea(10, 30);
        add(destination);
    }

    public void actionPerformed(ActionEvent e)
    {
        String temp;
        String btnName = e.getActionCommand();
        
        if (btnName.equals("Copy->"))
        {
            temp = source.getSelectedText();
            if ("".equals(destination.getText()))
                destination.setText(temp);
            else
                destination.append(temp);
        }
    }
    // You need to select text in order to copy
}
