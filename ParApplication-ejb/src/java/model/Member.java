
package model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Member implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String memberName;
    private boolean fullMember;

    public Member()
    {
    }

    public Member(String memberName, boolean fullMember)
    {
        this.memberName = memberName;
        this.fullMember = fullMember;
    }

    public static List<Member> getMembers(EntityManager anEmanager)
    {
        String qry = "select m from Member m";
        List<Member> members = anEmanager.createQuery(qry).getResultList();
        return members;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getMemberName()
    {
        return memberName;
    }

    public void setMemberName(String memberName)
    {
        this.memberName = memberName;
    }

    public boolean isFullMember()
    {
        return fullMember;
    }

    public void setFullMember(boolean fullMember)
    {
        this.fullMember = fullMember;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (int) id;
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        if (!(object instanceof Member))
        {
            return false;
        }
        Member other = (Member) object;
        if (this.id != other.id)
        {
            return false;
        }
        return true;
    }

    @Override
    public String toString()
    {
        return "model.Member[ id=" + id + " ]";
    }
}
