
package servletPackage;

import java.io.*;
import java.util.*;
import javax.servlet.*;
import javax.servlet.http.*;

public class DaytimeServlet extends HttpServlet implements DaytimeServer
{
    public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
    {
        // If the client says "format=object" then
        // send the Date as a serialized object
        if ("object".equals(req.getParameter("format")))
        {
            ObjectOutputStream out = new ObjectOutputStream(res.getOutputStream());
            out.writeObject(getDate());
        }
        // Otherwise send the Date as a normal ASCII string
        else
        {
            PrintWriter out = res.getWriter();
            out.println(getDate().toString());
        }
    }

    public void doPost(HttpServletRequest req, HttpServletResponse res)
            throws ServletException, IOException
    {
        doGet(req, res);
    }
    
    // The single method from DaytimeServer
    public Date getDate()
    {
        return new Date();
    }
}
