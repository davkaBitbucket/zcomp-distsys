
package exercisePackage;

import java.applet.Applet;
import java.awt.Button;
import java.awt.Choice;
import java.awt.List;
import java.awt.TextArea;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

public class Exercise7 extends Applet implements ActionListener, ItemListener
{
    Choice micro;
    List parts;
    Button done;
    TextArea spec;
    
    int[] procType = new int[4];
    float[] procCost = new float[4];
    int p = 0;
    String[] peripheral = new String[8];
    
    public void init() 
    {
        micro = new Choice();
        micro.addItemListener(this);
        micro.addItem("P1Mhz");
        micro.addItem("P1.8Mhz");
        micro.addItem("P2Mhz");
        micro.addItem("P2.3Mhz");
        micro.select(0);
        add(micro);
        
        procType[0] = 1000;
        procCost[0] = 85;
        procType[1] = 1800;
        procCost[1] = 95;
        procType[2] = 2000;
        procCost[2] = 135;
        procType[3] = 2300;
        procCost[3] = 165;
        
        peripheral[0] = "CD-R/DVD";
        peripheral[1] = "Sound card";
        peripheral[2] = "Speakers";
        peripheral[3] = "Tape Drive";
        peripheral[4] = "Zip Drive";
        peripheral[5] = "Modem";
        peripheral[6] = "Extra 64Mb RAM";
        peripheral[7] = "10Gb Hard Drive";
        
        parts = new List(6, true);
        parts.addItemListener(this);
        
        for (int i = 0; i < 8; i++)
        {
            parts.add(peripheral[i]);
        }
        add(parts);
        
        done = new Button("Done");
        done.addActionListener(this);
        add(done);
        spec = new TextArea(10, 30);
        add(spec);
    }

    public void actionPerformed(ActionEvent e)
    {
        p = micro.getSelectedIndex();
        spec.setText("Pentium " + procType[p] + " at £" + procCost[p] + "\n");
        for (int i = 0; i < 8; i++)
        {
            if(parts.isIndexSelected(i))
            {
                spec.append(peripheral[i] + "\n");
            }
        }
    }

    public void itemStateChanged(ItemEvent e)
    {
        
    }
}
