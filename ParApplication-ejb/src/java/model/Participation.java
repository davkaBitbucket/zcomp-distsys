
package model;

import java.io.Serializable;
import java.text.DateFormat;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;

@Entity
public class Participation implements Serializable
{
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @ManyToOne
    private Member member;
    @ManyToOne
    private Event event;
    private int totalFee;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date feeDueDate;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateAdded;
    boolean feesPaid;

    public Participation()
    {
    }

    public static List<Participation> getParticipations(EntityManager anEmanager)
    {
        String qry = "select p from Participation p";
        List<Participation> participations = anEmanager.createQuery(qry).
                getResultList();
        return participations;
    }

    @Override
    public String toString()
    {
        String strId = String.format("%-6s", id);
        String strEvent = String.format("%-25s", event.getId() + "  " + 
                event.getEventName());
        String strMember = String.format("%-20s", member.getId() + "  " + 
                member.getMemberName());
        String strTotalFee = String.format("%-15s", totalFee);
        String strDateAdded = String.format("%-20s", DateFormat.getInstance().
                format(dateAdded));
        String strFeeDueDate = String.format("%-20s", DateFormat.getInstance().
                format(feeDueDate));
        String strFeesPaid = Boolean.toString(feesPaid);
        return strId + strEvent + strMember + strTotalFee + strDateAdded + 
                strFeeDueDate + strFeesPaid;
    }

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public Member getMember()
    {
        return member;
    }

    public void setMember(Member member)
    {
        this.member = member;
    }

    public Event getEvent()
    {
        return event;
    }

    public void setEvent(Event event)
    {
        this.event = event;
    }

    public int getTotalFee()
    {
        return totalFee;
    }

    public void setTotalFee(int totalFee)
    {
        this.totalFee = totalFee;
    }

    public Date getFeeDueDate()
    {
        return feeDueDate;
    }

    public void setFeeDueDate(Date feeDueDate)
    {
        this.feeDueDate = feeDueDate;
    }

    public Date getDateAdded()
    {
        return dateAdded;
    }

    public void setDateAdded(Date dateAdded)
    {
        this.dateAdded = dateAdded;
    }

    public boolean isFeesPaid()
    {
        return feesPaid;
    }

    public void setFeesPaid(boolean feesPaid)
    {
        this.feesPaid = feesPaid;
    }

    @Override
    public int hashCode()
    {
        int hash = 0;
        hash += (int) id;
        return hash;
    }

    @Override
    public boolean equals(Object object)
    {
        if (!(object instanceof Participation))
        {
            return false;
        }
        Participation other = (Participation) object;
        if (this.id != other.id)
        {
            return false;
        }
        return true;
    }
}
