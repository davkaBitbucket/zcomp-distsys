
package appletPackage;

import java.applet.*;
import java.awt.*;
import java.io.*;
import java.net.*;
import java.util.*;

import com.oreilly.servlet.HttpMessage;

public class DaytimeApplet extends Applet
{
    static final int DEFAULT_PORT = 1313;
    TextField httpText, httpObject, socketText, socketObject, RMIObject;
    Button refresh;

    public void init()
    {
        setLayout(new BorderLayout());
        Panel west = new Panel();
        west.setLayout(new GridLayout(5, 1));
        west.add(new Label("HTTP text: ", Label.RIGHT));
        west.add(new Label("HTTP object: ", Label.RIGHT));
        west.add(new Label("Socket text: ", Label.RIGHT));
        west.add(new Label("Socket object: ", Label.RIGHT));
        west.add(new Label("RMI object: ", Label.RIGHT));
        add("West", west);

        // On the right create text fields to display the retrieved time values
        Panel center = new Panel();
        center.setLayout(new GridLayout(5, 1));

        httpText = new TextField();
        httpText.setEditable(false);
        center.add(httpText);

        httpObject = new TextField();
        httpObject.setEditable(false);
        center.add(httpObject);

        socketText = new TextField();
        socketText.setEditable(false);
        center.add(socketText);

        socketObject = new TextField();
        socketObject.setEditable(false);
        center.add(socketObject);

        RMIObject = new TextField();
        RMIObject.setEditable(false);
        center.add(RMIObject);

        add("Center", center);

        // On the bottom create a button to update the times
        Panel south = new Panel();
        refresh = new Button("Refresh");
        south.add(refresh);
        add("South", south);
    }

    public void start()
    {
        refresh();
    }

    private void refresh()
    {
        // Fetch and display the time values
        httpText.setText(getDateUsingHttpText());
        httpObject.setText(getDateUsingHttpObject());
    }

    private String getDateUsingHttpText()
    {
        try
        {
            // Construct a URL referring to the servlet
            URL url = new URL(getCodeBase(), "/DaytimeApplication/DaytimeServlet");

            // Create a com.oreilly.servlet.HttpMessage to communicate with that URL
            HttpMessage msg = new HttpMessage(url);

            // Send a GET message to the servlet, with no query string
            // Get the response as an InputStream
            InputStream in = msg.sendGetMessage();

            // Wrap the InputStream with a DataInputStream
            DataInputStream result =
                    new DataInputStream(new BufferedInputStream(in));

            // Read the first line of the response, which should be
            // a string representation of the current time
            String date = result.readLine();

            // Close the InputStream
            in.close();

            // Return the retrieved time
            return date;
        }
        catch (Exception e)
        {
            // If there was a problem, print to System.out
            // (typically the Java console) and return null
            e.printStackTrace();
            return null;
        }
    }

    private String getDateUsingHttpObject()
    {
        try
        {
            // Construct a URL referring to the servlet
            URL url = new URL(getCodeBase(), "/DaytimeApplication/DaytimeServlet");

            // Create a com.oreilly.servlet.HttpMessage to communicate with that URL
            HttpMessage msg = new HttpMessage(url);

            // Construct a Properties list to say format=object
            Properties props = new Properties();
            props.put("participation", "start");

            InputStream in = msg.sendGetMessage(props);
            ObjectInputStream result = new ObjectInputStream(in);

            // Read the Date object from the stream
            Object obj = result.readObject();
            Date date = (Date) obj;

            // Return the string representation of the Date
            return date.toString();
        }
        catch (Exception e)
        {
            // If there was a problem, print to System.out
            // (typically the Java console) and return null
            e.printStackTrace();
            return null;
        }
    }

    public boolean handleEvent(Event event)
    {
        // When the refresh button is pushed, refresh the display
        // Use JDK 1.0 events for maximum portability
        switch (event.id)
        {
            case Event.ACTION_EVENT:
                if (event.target == refresh)
                {
                    refresh();
                    return true;
                }
        }
        return false;
    }
}
