
package ejb;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.Event;
import model.Member;
import model.Participation;

@Stateless
public class ParticipationSystem implements ParticipationSystemRemote
{
    @PersistenceContext(unitName = "ParApplication-ejbPU")
    private EntityManager em;
    private Participation participation;
    private ArrayList<String> eventNames, memberNames, parDetails;
    private List<Event> events;
    private List<Member> members;
    private ArrayList<Participation> participations;
    boolean firstTime = true;

    @Override
    public void startParticipation()
    {
        participation = new Participation();
        events = Event.getEvents(em);
        members = Member.getMembers(em);
        
        eventNames = new ArrayList<String>();
        memberNames = new ArrayList<String>();
        parDetails = new ArrayList<String>();
        participations = new ArrayList<Participation>();
        for (int i = 0; i < events.size(); i++)
        {
            eventNames.add(events.get(i).getEventName());
        }
        for (int i = 0; i < members.size(); i++)
        {
            memberNames.add(members.get(i).getMemberName());
        }
    }

    @Override
    public ArrayList<String> getEventNames()
    {
        return eventNames;
    }

    @Override
    public ArrayList<String> getMemberNames()
    {
        return memberNames;
    }

    @Override
    public void selectEvent(int index)
    {
        Event event = events.get(index);
        participation.setEvent(event);
    }

    @Override
    public String selectMember(int index)
    {
        Member member = members.get(index);
        if (!member.isFullMember())
        {
            return "notfullmember";
        }
        else if (firstTime)
        {
            participation.setMember(member);
            participation.setDateAdded(getDate());
            participation.setFeeDueDate(getFeeDueDate());
            participation.setFeesPaid(false);
            participations.add(participation);
            parDetails.add(participation.toString());
            setTotalFee();
            firstTime = false;
        }
        else
        {
            Event event = participation.getEvent();
            participation = new Participation();
            participation.setEvent(event);
            participation.setMember(member);
            participation.setDateAdded(getDate());
            participation.setFeeDueDate(getFeeDueDate());
            participation.setFeesPaid(false);
            participations.add(participation);
            parDetails.add(participation.toString());
            setTotalFee();
        }
        return "memberadded";
    }

    @Override
    public ArrayList<String> getParDetails()
    {
        return parDetails;
    }

    public void setTotalFee()
    {
        int eventFee = participation.getEvent().getEventFee();
        int totalFee = participations.size() * eventFee;
        for (int i = 0; i < participations.size(); i++)
        {
            participations.get(i).setTotalFee(totalFee);
            parDetails.set(i, participations.get(i).toString()); 
        }
    }

    public Date getDate()
    {
        Date date = new Date();
        return date;
    }

    public Date getFeeDueDate()
    {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, 1);
        return cal.getTime();
    }

    @Override
    public void closeParticipation()
    {
        for (int i = 0; i < participations.size(); i++)
        {
            persist(participations.get(i));
        }
    }

    @Override
    public void addSampleData()
    {
        // Populating events into database
        Event event = new Event("Football playing", 10, "Play football "
                + "together fun!");
        persist(event);
        event = new Event("Basketball playing", 38, "Bring your friends");
        persist(event);
        event = new Event("Running", 5, "Raise donation by running");
        persist(event);
        event = new Event("Dance festival", 25, "Party for this semester");
        persist(event);
        event = new Event("Horse riding", 50, "Bring your lunch and "
                + "warm clothes");
        persist(event);
        // Populating members into database
        Member member = new Member("William Wallace", true);
        persist(member);
        member = new Member("Victor Duncan", true);
        persist(member);
        member = new Member("Mark Clarke", true);
        persist(member);
        member = new Member("Stuart Brady", false);
        persist(member);
    }

    public void persist(Object object)
    {
        em.persist(object);
    }
}
