
package exercisePackage;

import java.applet.Applet;
import java.awt.Button;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Exercise6 extends Applet implements ActionListener
{
    Button btnClick;
    TextField entry;
    Label prompt;
    String temp;
    // int x = (int)(java.lang.Math.random() * 100);
    int x = 110;
    int number;
    
    public void init() 
    {
        prompt = new Label("Guess the number");
        add(prompt);
        entry = new TextField(10);
        add(entry);
        btnClick = new Button("Check your guess");
        add(btnClick);
        btnClick.addActionListener(this);
    }
    
    public void actionPerformed(ActionEvent e)
    {
        temp = entry.getText();
        number = Integer.parseInt(temp);
        if (x > number)
            prompt.setText("Too low try again");
        else if (x < number)
            prompt.setText("Too high try again");
        else
            prompt.setText("You found it congrats");
    }
}
