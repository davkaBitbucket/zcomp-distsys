
package exercisePackage;

import java.applet.Applet;
import java.awt.Button;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Exercise2 extends Applet implements ActionListener
{
    Button btnRed, btnGreen;
    Color red = Color.red;
    Color green = Color.GREEN;
    Color current = red;
    
    public void init()
    {
        btnRed = new Button("Red");
        btnRed.setEnabled(false);
        btnRed.addActionListener(this);
        btnGreen = new Button("Green");
        btnGreen.addActionListener(this);
        add(btnRed);
        add(btnGreen);
        current = red;
    }
    
    public void actionPerformed(ActionEvent e)
    {
        String btnName = e.getActionCommand();
        if ("Red".equals(btnName))
        {
            btnRed.setEnabled(false);
            btnGreen.setEnabled(true);
            current = red;
        }
        if ("Green".equals(btnName))
        {
            btnRed.setEnabled(true);
            btnGreen.setEnabled(false);
            current = green;
        }
        repaint();
    }
    
    public void paint(Graphics g)
    {
        g.setColor(current);
        g.fillRect(100, 50, 200, 100);
    }
    
}
