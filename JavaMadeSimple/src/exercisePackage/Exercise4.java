
package exercisePackage;

import java.applet.Applet;
import java.awt.Label;
import java.awt.TextArea;
import java.awt.TextField;

public class Exercise4 extends Applet 
{
    TextField email;
    TextArea details;
    
    public void init() 
    {
        add(new Label("Your e-mail address"));
        email = new TextField(40);
        add(email);
        details = new TextArea("Enter your name and adress here", 6, 40);
        add(details);
        details.selectAll();
        validate();
    }
}
