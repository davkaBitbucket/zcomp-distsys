
package exercisePackage;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Button;
import java.awt.Canvas;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Panel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Exercise8 extends Applet implements ActionListener
{
    Panel fnames, styles;
    Button sans, serif;
    Button bold, bigger, smaller;
    SampleCanvas sample;
    String fName = "SansSerif";
    int fBold = 0;
    int fSize = 12;

    public void init()
    {
        setFont(new Font("Sans", Font.BOLD, 12));
        setLayout(new BorderLayout());

        fnames = new Panel();
        setLayout(new GridLayout(1, 2, 5, 10));
        sans = new Button("Sans Serif");
        sans.addActionListener(this);
        fnames.add(sans);
        serif = new Button("Serif");
        serif.addActionListener(this);
        fnames.add(serif);
        add(fnames, "South");

        styles = new Panel();
        styles.setLayout(new GridLayout(3, 1, 10, 5));
        bold = new Button("Bold");
        bold.addActionListener(this);
        styles.add(bold);
        bigger = new Button("Bigger");
        bigger.addActionListener(this);
        styles.add(bigger);
        smaller = new Button("Smaller");
        smaller.addActionListener(this);
        styles.add(smaller);
        add(styles, "West");

        sample = new SampleCanvas();
        add(sample, "Center");
        validate();
    }

    public void actionPerformed(ActionEvent e)
    {
        String btnName = e.getActionCommand();
        if (btnName.equals("Bold"))
        {
            if (fBold == 1)
                fBold = 0;
            else
                fBold = 1;
        }
        if (btnName.equals("Bigger"))
            fSize += 2;
        if (btnName.equals("Smaller"))
            fSize -= 2;
        if (btnName.equals("Serif"))
            fName = "Serif";
        if (btnName.equals("Sans Serif"))
            fName = "SansSerif";
        sample.rewrite(fName);
    }
    
    class SampleCanvas extends Canvas
    {
        String message;

        public SampleCanvas()
        {
            rewrite("SansSerif");
        }

        public void rewrite(String fontName)
        {
            setFont(new Font(fontName, fBold, fSize));
            message = "This font is " + fontName;
            repaint();
        }

        public void paint(Graphics g)
        {
            g.drawString(message, 10, 50);
        }
    }
}